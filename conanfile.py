from conans import ConanFile, tools, AutoToolsBuildEnvironment

import os
from os import path

import shutil

class Recipe(ConanFile):
    name           = "libdb"
    description    = "a library intended to provide a scalable high-performance " \
                   + "embedded key-value database"
    license        = "BSD 3-clause (modified)"

    version        = "4.8.30"
    settings       = "os", "compiler", "arch"

    homepage       = "http://www.oracle.com/technetwork/database/database-technologies/berkeleydb/overview/index.html"
    source_sha256  = "e0491a07cdb21fb9aa82773bbbedaeb7639cbd0e7f96147ab46141e0045db72a"

    url            = "https://gitlab.com/0-face/conan-recipes/libdb-conan.git"

    def source(self):
        zip_name = self.name + ".tar.gz"
        url = "http://download.oracle.com/berkeley-db/db-{}.tar.gz".format(self.version)

        zip_path = path.join(self.source_folder, zip_name)

        if path.exists(zip_path) and tools.sha256sum(zip_path) == self.source_sha256:
            self.output.info("Skipped source download ('%s' found)" % zip_name)
        else:
            self.output.info("Downloading source code ...")
            tools.download(url, zip_name)
            tools.check_sha256(zip_name, self.source_sha256)

        self.output.info("Extracting zip...")
        tools.unzip(zip_name)

    def build(self):
        self.try_make_dir(self.build_dir)
        self.try_make_dir(self.install_dir)

        with tools.chdir(self.build_dir):
            autotools = AutoToolsBuildEnvironment(self)
            autotools.configure(
                configure_dir=self.configure_dir,
                args=[
                    "--enable-compat185",
                    "--prefix=" + self.install_dir
                ]
            )
            autotools.make()
            autotools.install()

    def package(self):
        self.copy("*", src=self.install_dir_name)

        self.copy("LICENSE", src=self.extracted_folder_name)

    def package_info(self):
        self.cpp_info.libs = ['db']

################################# Helpers ########################################################

    def run_and_log(self, cmd, cwd=None, env=None):
        msg = ''
        if cwd:
            msg = "cwd='{}'\n".format(cwd)
        if env:
            msg += "env='{}'\n".format(env)

        if cwd or env:
            msg += "\t"

        self.output.info(msg + cmd)

        self.run(cmd, cwd = cwd)

    def try_make_dir(self, directory):
        import errno

        try:
            os.makedirs(directory)
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise

######################################## Properties ####################################################

    @property
    def build_dir(self):
        return path.join(self.build_folder, 'build')

    @property
    def install_dir_name(self):
        return 'install'

    @property
    def install_dir(self):
        return path.join(self.build_folder, self.install_dir_name)

    @property
    def configure_dir(self):
        return path.join(self.extracted_folder, 'dist')

    @property
    def extracted_folder_name(self):
        return 'db-{}'.format(self.version)

    @property
    def extracted_folder(self):
        return path.join(self.source_folder, self.extracted_folder_name)
