# Berkeley DB - conan recipe

[Conan.io][conan_site] recipe to build [Berkeley DB][homepage] library.

[conan_site]: https://www.conan.io/
[homepage]: http://www.oracle.com/technetwork/database/database-technologies/berkeleydb/overview/index.html


## About Berkeley DB

> Berkeley DB is a family of embedded key-value database libraries providing
  scalable high-performance data management services to applications.


## How to use

See [conan docs][conan_docs] for instructions in how to use conan.
To know how to use the library, you can look for the docs included with the
package (or the source code). Documentation for the most recent version
of the library are available [here][library_docs]

[conan_docs]: http://docs.conan.io/en/latest/
[library_docs]: https://docs.oracle.com/database/bdb181/index.html


## License

This conan recipe is distributed under the [unlicense](http://unlicense.org/) terms
(see UNLICENSE.md).

The library is distributed under a modified version of BSD 3-clause license.
We reproduce below part of the license (full file are included with the
package and the source code).

```
...

/*
* Copyright (c) 1990-2009 Oracle.  All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
* 1. Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the distribution.
* 3. Redistributions in any form must be accompanied by information on
*    how to obtain complete source code for the DB software and any
*    accompanying software that uses the DB software.  The source code
*    must either be included in the distribution or be available for no
*    more than the cost of distribution plus a nominal fee, and must be
*    freely redistributable under reasonable conditions.  For an
*    executable file, complete source code means the source code for all
*    modules it contains.  It does not include source code for modules or
*    files that typically accompany the major components of the operating
*    system on which the executable file runs.
*
* THIS SOFTWARE IS PROVIDED BY ORACLE ``AS IS'' AND ANY EXPRESS OR
* IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR
* NON-INFRINGEMENT, ARE DISCLAIMED.  IN NO EVENT SHALL ORACLE BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
* BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
* WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

...
```
